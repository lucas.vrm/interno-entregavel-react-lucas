import React from 'react';
import firebase from './firebase';
import './App.css';
import CardEmocao from './components/CardEmocao';

function App() {
  const [emocoes, setEmocoes] = React.useState([]);

  React.useEffect(() => {
    const fetchData = async () => {
      const db = firebase.firestore();
      const data = await db.collection('emocoes').get();
      setEmocoes(data.docs.map(doc => ({ ...doc.data(), id: doc.id})));
    };
    fetchData();
  }, []);

  return (
    <div>
      <CardEmocao/>
      <ul>
        {
          emocoes.map(emocao => (
            <li key={emocao.id}>

            </li>
          ))
        }
      </ul>
    </div>
  );
}

export default App;
