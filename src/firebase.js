import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyDTx5RP9dwWab-R7EjFgKK5xN3QYjqC1-s",
    authDomain: "diario-789ae.firebaseapp.com",
    databaseURL: "https://diario-789ae.firebaseio.com",
    projectId: "diario-789ae",
    storageBucket: "diario-789ae.appspot.com",
    messagingSenderId: "1018801597785",
    appId: "1:1018801597785:web:c2843d0452cf5f1000b048",
    measurementId: "G-GFJCPCKYM6"
  };
	// Initialize Firebase
  firebase.initializeApp(firebaseConfig);

	export default firebase;
